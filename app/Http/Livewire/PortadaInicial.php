<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PortadaInicial extends Component
{
    public function render()
    {
        return view('livewire.portada-inicial');
    }
}
