<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TecnologiasQueUtilizamos extends Component
{
    public function render()
    {
        return view('livewire.tecnologias-que-utilizamos');
    }
}
