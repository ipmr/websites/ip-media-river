<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CasosDeExito extends Component
{
    public function render()
    {
        return view('livewire.casos-de-exito');
    }
}
