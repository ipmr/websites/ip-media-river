<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MenuDeServiciosInicial extends Component
{
    public function render()
    {
        return view('livewire.menu-de-servicios-inicial');
    }
}
