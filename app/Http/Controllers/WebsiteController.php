<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{
	public function index(){
		return view('website.index');
	}
	public function acerca_de(){
		return view('website.acerca-de');
	}
}
