<header class="px-7 py-10 bg-red-900 portada-inicial">
	<div class="block md:flex items-center text-center md:text-left">
		<a href="{{ route('inicio') }}" class="text-white inline-block mb-8 md:m-0">
			<img src="{{ asset('img/ipmediariver_logo.svg') }}" class="logo" alt="">
		</a>
		<nav class="ml-auto flex space-x-5 md:space-x-14 navegacion justify-center md:justify-end">
			<a href="{{ route('inicio') }}">Inicio</a>
			<a href="{{ route('acerca-de') }}">Nosotros</a>
			<a href="https://api.whatsapp.com/send?phone=526643864368" target="_blank" class="text-white">
				<i class="fab fa-whatsapp mr-2"></i>
				<b class="hidden md:inline-block">Escríbenos por WhatsApp</b>
				<b class="inline-block md:hidden">Escríbenos</b>
			</a>
		</nav>
	</div>
	<div class="container block md:flex items-center mx-auto pt-12 md:pt-24 pb-7 md:pb-15 text-center md:text-left">
		{{ $contenido }}
	</div>
</header>