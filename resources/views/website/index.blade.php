<x-guest-layout>
	@livewire('portada-inicial')
	@livewire('menu-de-servicios-inicial')
	@livewire('casos-de-exito')
	@livewire('tecnologias-que-utilizamos')
	@livewire('google-maps')
</x-guest-layout>