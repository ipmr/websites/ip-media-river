<x-guest-layout>
	
	<x-website-header>
		<x-slot name="contenido">
			<div class="w-full md:w-1/2">
				<h1 class="titulo text-white">Acerca de nosotros</h1>
			</div>
			<div class="w-2/3 md:w-1/3 mx-auto md:mr-0">
				<img src="{{ asset('img/undraw/undraw_informed_decision_p2lh.svg') }}" class="mt-12 md:mt-0" alt="">
			</div>
		</x-slot>
	</x-website-header>

	<section class="py-20 px-4">
		<div class="container block md:flex items-start mx-auto text-center md:text-left">
			<div class="w-full md:w-1/3 hidden md:block">
				<p class="subtitulo text-red-900 mb-7 md:mb-0">Nosotros</p>
			</div>
			<div class="w-full md:w-2/3">
				<p class="lead mb-12">
					IP Media River inicia en el año 2009 en Tijuana Baja California como una agencia de diseño gráfico, marketing digital y desarrollo web con el mismo entusiasmo que hasta ahora mantenemos.
				</p>
				<p class="lead mb-12">Los avances en la tecnología nos convirtieron en pioneros de lo que ahora es la web moderna, seguimos con la idea de las buenas practicas en el desarrollo y el equilibrio perfecto en nuestras interfaces gráficas.</p>
				<p class="lead mb-12">Es por ello que nuestros clientes a través de los años han quedado completamente satisfechos con nuestros servicios y productos que desarrollamos exclusivamente para cada uno de ellos, con el detalle y la calidad que se merecen.</p>
			</div>
		</div>
	</section>

</x-guest-layout>