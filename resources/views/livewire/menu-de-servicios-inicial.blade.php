<section class="py-10 px-4 md:py-15">
	<div class="container mx-auto text-center">
		<div class="flex">
			<div class="mx-auto w-full md:w-1/2">
				<h1 class="titulo">Conoce nuestros servicios</h1>
				<p class="lead text-gray-600 mt-7">
					En IP Media River nos enfocamos en darle vida a productos que automatizan negocios desde los más pequeños hasta grandes corporativos.
				</p>
			</div>
		</div>

		<div class="block md:flex items-stretch space-y-7 md:space-y-0 space-x-0 md:space-x-10 mt-14">

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-project-diagram fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">ERP</h3>
					<p class="text-lg mb-4"><b>Planificación de recursos empresariales</b></p>
					<p class="text-gray-600">
						Integración de sistemas de las diferentes operaciones de tu empresa
					</p>
				</div>
			</div>

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-funnel-dollar fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">CRM</h3>
					<p class="text-lg mb-4"><b>Gestión de relación con el cliente</b></p>
					<p class="text-gray-600">
						Organiza la información de tus cuentas y contactos para hacer mucho más simple tu proceso de ventas.
					</p>
				</div>
			</div>

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-headset fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">HELPDESK</h3>
					<p class="text-lg mb-4"><b>Tickets de servicio</b></p>
					<p class="text-gray-600">
						Atención efectiva a los requerimientos de tus clientes y reportes de servicio personalizados
					</p>
				</div>
			</div>

		</div>

		<div class="block md:flex items-stretch space-y-7 md:space-y-0 space-x-0 md:space-x-10 mt-14">

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-chart-line fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">BI</h3>
					<p class="text-lg mb-4"><b>Business Intelligence</b></p>
					<p class="text-gray-600">
						Transforma tus datos en conocimiento que te ayude a tomar las mejores decisiones para lograr tus objetivos.
					</p>
				</div>
			</div>

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-file-code fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">PROCESOS</h3>
					<p class="text-lg mb-4"><b>Análisis e integración de procesos</b></p>
					<p class="text-gray-600">
						Mantén todos tus sistemas documentados para asegurar la calidad de tus procesos o desarrollos.
					</p>
				</div>
			</div>

			<div class="w-full md:w-1/3">
				<div class="tarjeta h-full">
					<i class="fa fa-code fa-3x text-red-900 mb-7"></i>
					<h3 class="subtitulo text-gray-400 mb-3">PÁGINAS WEB</h3>
					<p class="text-lg mb-4"><b>Oficinas virtuales</b></p>
					<p class="text-gray-600">
						Permite que tus clientes te visiten 24 horas los 7 días de la semana, estar en Internet es parte fundamental para el crecimiento de tu empresa.
					</p>
				</div>
			</div>

		</div>
	</div>
</section>
