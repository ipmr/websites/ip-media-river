<section class="py-10 md:py-20 px-4 bg-white">
	<div class="container mx-auto">
		<div class="flex">
			<div class="mx-auto w-full md:w-1/2 text-center">
				<h1 class="titulo">Casos de éxito</h1>
				<p class="lead text-gray-600 mt-7">
					Nos sentimos orgullosos de trabajar al lado de clientes que buscan la innovación en sus empresas y que nos permiten crecen juntos con ellos en experiencia y en calidad en todo lo que hacemos. A continuación detallamos algunas características de nuestros trabajos.
				</p>
			</div>
		</div>

		<div class="block md:flex items-center space-x-0 md:space-x-24 space-y-10 md:space-y-0 mt-20 text-center md:text-left">
			<div class="w-full md:w-1/3 hidden md:block">
				<img src="{{ asset('img/undraw/undraw_Analytics_re_dkf8.svg') }}" class="w-full h-auto" alt="">
			</div>
			<div class="w-full md:w-1/2">
				<h3 class="text-xl font-bold text-red-900">Costos de calidad</h3>
				<h1 class="text-3xl font-bold mb-7">Monitoreo y Análisis</h1>
				<p class="mb-5">Aplicación web para la gestión de costos de calidad en las distintas naves industriales del cliente, la aplicación tiene la capacidad de generar reportes de análisis y comparación de años fiscales descargables en PDF y XLS así como un REST API para integración con las diferentes aplicaciones del cliente.</p>
			</div>
			<div class="w-full md:w-1/2 block md:hidden">
				<img src="{{ asset('img/undraw/undraw_Analytics_re_dkf8.svg') }}" class="w-full h-auto" alt="">
			</div>
		</div>

		<div class="block md:flex items-center space-x-0 md:space-x-24 space-y-10 md:space-y-0 mt-20 text-center md:text-left">
			<div class="w-full md:w-1/2">
				<h3 class="text-xl font-bold text-red-900">Reporte de incidentes</h3>
				<h1 class="text-3xl font-bold mb-7">Seguridad Perimetral</h1>
				<p class="mb-5">Sistema de registro de incidencias capturados por usuarios designados en distintas zonas, las incidencias se categorizan y se genera un reporte automático que se envía a los contactos de cada zona para el control y cuidado de la misma.</p>
			</div>
			<div class="w-full md:w-1/3">
				<img src="{{ asset('img/undraw/undraw_secure_data_0rwp.svg') }}" class="w-full h-auto" alt="">
			</div>
		</div>

	</div>
</section>
