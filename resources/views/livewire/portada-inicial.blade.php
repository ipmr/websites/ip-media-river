<x-website-header>
	<x-slot name="contenido">
		<div class="w-full md:w-1/2 mb-10 md:m-0">
			<h1 class="text-2xl md:text-5xl font-bold mb-7 text-white">
				Diseño y Desarrollo de Aplicaciones Web a la Medida
			</h1>
			<p class="text-xl text-white text-opacity-75">
				Desde el 2009 estamos ubicados en Tijuana B.C. desarrollando aplicaciones web para automatizar procesos y aumentar la productividad de nuestros clientes.
			</p>
			<a href="{{ route('acerca-de') }}" class="btn-secondary mt-12 shadow">Leer más sobre nosotros</a>
		</div>
		<div class="w-full md:w-1/2">
			<img src="{{ asset('img/undraw/undraw_progressive_app_m9ms.svg') }}" class="w-full h-auto" alt="">
		</div>
	</x-slot>
</x-website-header>