<footer class="py-10 md:py-20 bg-white border-t border-red-800 px-4 text-center md:text-left">
	<div class="container mx-auto block md:flex items-center space-x-0 md:space-x-32">
		<div class="w-full md:w-1/2 mb-10 md:m-0">
			<img src="{{ asset('img/ipmediariver_logo_color.svg') }}" class="mx-auto md:ml-0 w-56 h-auto mb-10" alt="">
			<h1 class="subtitulo mb-10">¿Te gustaría hablarnos de tu proyecto?</h1>
			<a href="https://api.whatsapp.com/send?phone=526643864368" target="_blank" class="btn-primary"> 
				<i class="fab fa-whatsapp mr-2"></i> Escríbenos por WhatsApp
			</a>
			<p class="mt-10 hidden md:block">© {{ now()->format('Y') }} {{ env('app_name') }} - Derechos Reservados.</p>
		</div>
		<div class="w-full md:w-1/2">
			<h1 class="subtitulo mb-7">Visítanos</h1>
			<p class="text-lg">Estamos ubicados en: Blvd. Agua Caliente 4558 Piso 5 Local 505, Aviación, 22014 Tijuana, B.C.</p>

			<p class="mt-10 font-bold mb-4">¿Eres programador?</p>
			<p>Trabaja con nosotros, envia tu CV <a href="mailto:info@ipmriver.com" class="text-red-900">aqui</a> y te responderemos en la brevedad posible.</p>
		</div>
		<p class="mt-10 block md:hidden mt-10">© {{ now()->format('Y') }} {{ env('app_name') }} - Derechos Reservados.</p>
	</div>
</footer>
