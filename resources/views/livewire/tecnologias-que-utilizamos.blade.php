<section class="bg-red-900 py-10 md:py-20 px-4">
	<div class="container mx-auto">
		<div class="flex">
			<div class="w-full md:w-2/3 mx-auto text-center">
				<img src="{{ asset('img/undraw/undraw_static_assets_rpm6.svg') }}" class="w-2/3 md:w-1/3 mx-auto mb-7 h-auto" alt="">
				<h1 class="titulo text-white mb-7">
					Tecnología que utilizamos
				</h1>
				<p class="text-xl text-white text-opacity-75">
					Siempre buscamos adaptarnos a las necesidades de nuestros clientes en cuanto a la tecnologia que utilizamos, mantenemos una constante actualización en nuestro conocimiento para asegurar darle a nuestros clientes software con tecnología de punta.
				</p>
				<div class="flex items-stretch flex-wrap md:flex-nowrap text-white mt-15 justify-center space-x-4 md:space-x-7">
					<i class="mb-5 fab fa-html5 fa-3x" title="HTML5"></i>
					<i class="mb-5 fab fa-css3-alt fa-3x" title="CSS3"></i>
					<i class="mb-5 fab fa-php fa-3x" title="PHP"></i>
					<i class="mb-5 fab fa-python fa-3x" title="PYTHON"></i>
					<i class="mb-5 fab fa-laravel fa-3x" title="LARAVEL FRAMEWORK"></i>
					<i class="mb-5 fab fa-vuejs fa-3x" title="VUE JS"></i>
					<i class="mb-5 fab fa-react fa-3x" title="REACT JS"></i>
					<i class="mb-5 fab fa-angular fa-3x" title="ANGULAR JS"></i>
				</div>
			</div>
		</div>
	</div>	
</section>
