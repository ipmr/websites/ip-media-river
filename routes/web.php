<?php
use App\Http\Controllers\WebsiteController;
use Illuminate\Support\Facades\Route;

Route::get('/', [WebsiteController::class, 'index'])->name('inicio');
Route::get('/acerca-de', [WebsiteController::class, 'acerca_de'])->name('acerca-de');
Route::redirect('login', '/');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
